var express = require('express');
const { route } = require('../app');
var router = express.Router();
var Cart = require('../models/cart');


var Product = require('../models/product');
/* GET home page. */
router.get('/', function(req, res, next) {
  Product.find({status:true},function(err,docs){
    var productChunks = [];
    var chunkSize = 3;
    for(var i = 0;i<docs.length;i+=chunkSize){
      productChunks.push(docs.slice(i,i+chunkSize));
    }
  
    
  res.render('plp', { 
    title: 'Express' ,
    products: productChunks,
    style: 'plp.css',
    head : 'Modern and Contempeory Art for Sale',
    message : req.flash('message')
  });
});
});

router.get('/plp', function(req, res, next) {
  Product.find({status:true},function(err,docs){
    var productChunks = [];
    var chunkSize = 3;
    for(var i = 0;i<docs.length;i+=chunkSize){
      productChunks.push(docs.slice(i,i+chunkSize));
    }
  
  res.render('plp', { 
    title: 'Express' ,
    products: productChunks,
    style: 'plp.css',
    head : 'Modern and contempeory art for sale'
  });
});
});

router.get('/painting', function(req, res, next) {
  Product.find({category:"Painting"},function(err,docs){
    var productChunks = [];
    var chunkSize = 3;
    for(var i = 0;i<docs.length;i+=chunkSize){
      productChunks.push(docs.slice(i,i+chunkSize));
    }
  
  res.render('plp', { 
    title: 'plp' ,
    products: productChunks,
    style: 'plp.css',
    head : 'Painting'
  });
});
});

router.get('/sculpters', function(req, res, next) {
  Product.find({category:"Sculpter"},function(err,docs){
    var productChunks = [];
    var chunkSize = 3;
    for(var i = 0;i<docs.length;i+=chunkSize){
      productChunks.push(docs.slice(i,i+chunkSize));
    }
  
  res.render('plp', { 
    title: 'plp' ,
    products: productChunks,
    style: 'plp.css',
    head : 'Sculpters'
  });
});
});

router.get('/drawing', function(req, res, next) {
  Product.find({category:"Drawing"},function(err,docs){
    var productChunks = [];
    var chunkSize = 3;
    for(var i = 0;i<docs.length;i+=chunkSize){
      productChunks.push(docs.slice(i,i+chunkSize));
    }
  
  res.render('plp', { 
    title: 'plp' ,
    products: productChunks,
    style: 'plp.css',
    head : 'Drawing'
  });
});
});


router.get('/pdp/:id', function(req, res, next) {
  Product.findById(req.params.id,function(err,docs){

  
  res.render('pdp', { 
    title: 'PDP' ,
    products: docs,
    style: 'pdp.css'
  });
});
});

router.get('/add-to-cart/:id', function(req, res, next) {
  var productId = req.params.id;
  var cart = new Cart (req.session.cart ? req.session.cart : {});

  Product.findById(productId, function(err, product){
  
    if(cart.items[product.id])
    {
      console.log("alreadty in cart");
      req.flash('message','item already in cart');
      res.redirect('/');
    }
    else
    {
    cart.add(product, product.id);
    req.session.cart = cart;
    console.log(req.session.cart);
    res.redirect('/');
    }
    
  });
});


router.get('/cart',function(req,res,next){
  if(!req.session.cart){
    return res.render('cart',{products:null});
  }
  var cart = new Cart(req.session.cart);
  res.render('cart',{products:cart.generateArray(), totalPrice: cart.totalPrice});
});


router.get('/delete-cart/:id',function(req,res,next){
  var productId= req.params.id;
  var cart = new Cart(req.session.cart ? req.session.cart :{});
  cart.deleteArray(productId);
  req.session.cart = cart;
  res.redirect('/cart');
});

module.exports = router;
