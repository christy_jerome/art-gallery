var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    imagePath: {type: Array, required: true},
    title: {type: String, required: true},
    artist: {type: String, required: true},
    description: {type: String, required: true},
    dimension: {type: String, required: true},
    category: {type: String, required: true},
    price: {type: Number, required: true},
    status: {type: Boolean, required: true},
    technique: {type: String, required: true}
});

module.exports = mongoose.model('Product', schema);